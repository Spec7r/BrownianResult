#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <stdio.h> 
#include <time.h>
#include <fstream>
#include <stdlib.h>
#include <cuda.h>
#include <curand_kernel.h>
#include <curand_mtgp32_host.h>
#include <curand_mtgp32dc_p_11213.h>
#include <Windows.h>

#define Blocks 64
#define Threads 1

struct Data
{
	double dt;
	double tau_L;
	double lambda;
	double E;
	double kT;
};

#define CURAND_CALL(x) do { if((x) != CURAND_STATUS_SUCCESS) { \
    printf("Error at %s:%d\n", __FILE__, __LINE__); \
    return EXIT_FAILURE; }} while (0)

#define CUDA_CALL(x) do { if((x) != cudaSuccess) { \
    printf("Error %s at %s:%d\n", cudaGetErrorString(x), __FILE__, __LINE__); \
    return EXIT_FAILURE;  }} while (0)

__global__ void generate_kernel(curandStateMtgp32 *devMTGPStates, Data data, int n, double *particle, int *result)
{
	int id = threadIdx.x + blockIdx.x * blockDim.x;

	double disp_x = 2.0 * data.lambda * data.kT;
	double x_dag = 2.0 * sqrt(data.E * data.lambda);
	double f1 = exp(-data.dt / data.tau_L);
	double f2 = sqrt(disp_x * (1.0 - f1 * f1));

	bool flag;

	while (true)
	{
		flag = true;

		for (int j = 0; j < n; j++)
		{
			if (abs(particle[id * n + j]) >= x_dag) continue;

			particle[id * n + j] = particle[id * n + j] * f1 + curand_normal(&devMTGPStates[blockIdx.x]) * f2;

			result[id * n + j]++;

			flag = false;
		}
		
		if (flag == true) break;
	}
	__syncthreads();
}

using namespace std;

int main(int argc, char *argv[])
{
	Data data;
	data.dt = 0.1;
	data.tau_L = 1.0;
	data.lambda = 1.0;
	data.E = 1.0;
	data.kT = 0.5;
	int sampleCount = 100;

	curandStateMtgp32 *devMTGPStates;
	mtgp32_kernel_params *devKernelParams;
	int *devResults, *hostResults;
	double *particle;

	LARGE_INTEGER t1, t2, f;

	//���������� ��� ����������
	static int idum;
	int seed;

	//������ ������ ��� ������ � ����
	ofstream foutP;
	ofstream foutR;

	srand(time(0));
	time((time_t *)&idum);
	seed = idum;

	// ��������� ������ � ������
	foutP.open("resultP.txt", ios::app);
	foutR.open("resultR.txt", ios::app);

	//��������� ����� ��� ����������� �� ���������� 
	hostResults = (int *)calloc(sampleCount * Blocks * Threads, sizeof(int));

	//��������� ����� ��� ����������� �� ����������� ���������� 
	CUDA_CALL(cudaMalloc((void **)&devResults, sampleCount * Blocks * Threads * sizeof(int)));
	CUDA_CALL(cudaMalloc((void **)&particle, sampleCount * Blocks * Threads * sizeof(double)));

	//���������� 0 
	CUDA_CALL(cudaMemset(devResults, 0, sampleCount * Blocks * Threads * sizeof(int)));
	CUDA_CALL(cudaMemset(particle, 0, sampleCount * Blocks * Threads * sizeof(double)));

	// Allocate space for prng states on device 
	//������������ ������������ ��� prng ��������� �� ���������� 
	CUDA_CALL(cudaMalloc((void **)&devMTGPStates, Blocks * sizeof(curandStateMtgp32)));

	// Setup MTGP prng states 
	//��������� ��������� prng MTGP   
	// Allocate space for MTGP kernel parameters 
	//������������ ������������ ��� ���������� ���� MOTOGP 
	CUDA_CALL(cudaMalloc((void**)&devKernelParams, sizeof(mtgp32_kernel_params)));

	// Reformat from predefined parameter sets to kernel format, 
	//����������������� �� ���������������� ������� ���������� � ������ ����, 
	// and copy kernel parameters to device memory 
	//� ����������� ��������� ���� � ������ ����������
	CURAND_CALL(curandMakeMTGP32Constants(mtgp32dc_params_fast_11213, devKernelParams));

	// Initialize one state per thread block
	//���������������� ���� ��������� ��� ������� ����� ������
	CURAND_CALL(curandMakeMTGP32KernelState(devMTGPStates, mtgp32dc_params_fast_11213, devKernelParams, Blocks, seed));

	//���������� �������
	QueryPerformanceCounter(&t1);

	generate_kernel << <Blocks, Threads >> > (devMTGPStates, data, sampleCount, particle, devResults);

	//���������� ��������� �������
	QueryPerformanceCounter(&t2);
	QueryPerformanceFrequency(&f);
	double search_time = double(t2.QuadPart - t1.QuadPart) / f.QuadPart;

	//Copy device memory to host
	//����������� � ������ ���������� �� ����
	CUDA_CALL(cudaMemcpy(hostResults, devResults, sampleCount * Blocks * Threads * sizeof(int), cudaMemcpyDeviceToHost));

	//Result
	int max = 0;
	for (int i = 0; i < sampleCount * Blocks * Threads; i++)
	{
		if (hostResults[i] > max)
		{
			max = hostResults[i];
		}
	}

	int result;
	//for (int i = 1; i <= max; i++)
	//{
	//	result = 0;

	//	for (int j = 0; j < M * Blocks * Threads; j++)
	//	{
	//		if (hostResults[j] == i)
	//		{
	//			result++;
	//		}
	//	}
	//	if (result > 0)
	//	{
	//		printf("%u %u \n", i, result);
	//	}

	//	fout << i << " " << result << endl;
	//}

	//for (int i = 0; i <M * Blocks * Threads; i++)
	//{

	//		//fout << i << " " << hostResults[i] << endl;
	//		printf("%u \n", hostResults[i]);
	//	
	//}

	int sumResult = 0;
	for (int i = 1; i <= max; i++)
	{
		result = 0;

		for (int j = 0; j < sampleCount * Blocks * Threads; j++)
		{
			if (hostResults[j] == i)
			{
				result++;
			}
		}
		if (result > 0)
		{
			printf("%u %u \n", i, result);
		}

		sumResult += result;

		foutP << i * data.dt << " " << (double)sumResult / (double)(sampleCount * Blocks * Threads) << endl;

		foutR << i * data.dt << " " << 1.0 - (double)((double)sumResult / (double)(sampleCount * Blocks * Threads)) << endl;
	}

	printf("%f \n", search_time);

	CUDA_CALL(cudaFree(devMTGPStates));
	CUDA_CALL(cudaFree(devResults));
	free(hostResults);

	return EXIT_SUCCESS;
}


